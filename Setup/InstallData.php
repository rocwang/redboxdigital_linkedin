<?php

namespace RedboxDigital\LinkedIn\Setup;

use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;


class InstallData implements InstallDataInterface
{

    private $customerSetupFactory;
    private $attributeSetFactory;

    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }

    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        $customerSetup = $this->customerSetupFactory->create(
            ['setup' => $setup]
        );
        $customerEntity = $customerSetup->getEavConfig()->getEntityType(
            'customer'
        );
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        $customerSetup->addAttribute(
            Customer::ENTITY,
            'linkedin_profile',
            [
                'type'           => 'varchar',
                'label'          => 'Linkedin Profile',
                'input'          => 'text',
                'required'       => false,
                'visible'        => true,
                'user_defined'   => true,
                'position'       => 10,
                'system'         => false,
                'frontend_class' => 'validate-url validate-length maximum-length-250',
                'validate_rules' => '{"max_text_length":"250","input_validation":"url"}',
                'global'         => 1,
                'unique'         => true,
            ]
        );

        $attribute = $customerSetup->getEavConfig()->getAttribute(
            Customer::ENTITY, 'linkedin_profile'
        );

        // Possible options for "used_in_forms":
        // [
        //     'adminhtml_checkout',
        //     'adminhtml_customer',
        //     'adminhtml_customer_address',
        //     'customer_account_create',
        //     'customer_account_edit',
        //     'customer_address_edit',
        //     'customer_register_address',
        // ]
        $attribute->addData(
            [
                'attribute_set_id'   => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms'      =>
                    [
                        'adminhtml_checkout',
                        'adminhtml_customer',
                        'customer_account_create',
                        'customer_account_edit',
                    ],
            ]
        );

        $attribute->save();

        $setup->endSetup();
    }
}
